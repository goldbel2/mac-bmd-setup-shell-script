# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository allows you to rapidly setup your mac for python and R development. 
* The proxy.sh script was taken from the depths of confluence (if you find a link, please submit a pull request for the readme).

### How do I get set up? ###

* You're just getting setup - download the repository
* Navigate to the location with the terminal
* Type in the terminal ```./mac-bmd-setup.sh ```
* Trouble shoot anything silly that happens


### Contribution guidelines ###

* Submit a pull request if you think of anything else critical

### Who do I talk to? ###

* Eli Goldberg (eli.goldberg@novartis.com) is the repo owner.