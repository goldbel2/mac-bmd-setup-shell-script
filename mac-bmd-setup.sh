#!/bin/bash
# make sure to set $ chmod 700 mac-bmd-setup.sh prior to running script
chmod 700 proxy.sh
echo proxy.sh
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" 
brew --config
export PATH="/usr/local/sbin:$PATH"
export HTTP_PROXY='http://usca-proxy01.na.novartis.net:2011'
export HTTPS_PROXY='http://usca-proxy01.na.novartis.net:2011'
export NO_PROXY='*.novartis.net'
brew doctor
brew update
brew install python3
brew install python
brew install caskroom/cask/pycharm-ce
brew install git
brew install caskroom/cask/rstudio
pip3 install matplotlib
pip3 install scipy
git config --global user.name "Eli Goldberg"
git config --global user.email eli.goldberg@novartis.com