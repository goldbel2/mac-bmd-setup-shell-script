#!/bin/sh
onNIBRNetwork=`nslookup nibr-proxy.global.nibr.novartis.net | grep -v find | grep nibr-proxy | wc -l | tr -d ' '`
if [ "$onNIBRNetwork" -eq "0" ]
then
	echo "not on the nibr network, unsetting proxy"
	unset http_proxy
	unset https_proxy
else
	echo "on the nibr network, setting proxy"
	export http_proxy="http://nibr-proxy.global.nibr.novartis.net:2011"
	export https_proxy="http://nibr-proxy.global.nibr.novartis.net:2011"
fi